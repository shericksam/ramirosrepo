<!DOCTYPE html5>
<html lang="">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Ramiro</title>

		<!-- Bootstrap CSS -->
		<meta name="csrf-token" content="{{ csrf_token() }}">
		<link rel="stylesheet" href="/css/app.css">
	</head>
	<body>
		<div class="container" align="center">
			<form action="" method="POST" role="form">
				<legend>Formulario :v</legend>
			
				<div class="form-group">
					<label for="">Nombre:</label>
					<input type="text" class="form-control" name="nombre" id="nombre" placeholder="Nombre">
				</div>
				<button type="" id='guardar' class="btn btn-primary">Guardar</button>
			</form>
		</div>

		<!-- jQuery & Bootstrap JavaScript-->
		<script src="/js/app.js"></script>
		<script>
			$('#guardar').click(function (event) {
		         event.preventDefault();
		         var _token = $("meta[name='csrf-token']").attr("content");
		        $nombre = $("#nombre").val();
		        
		        $.ajax({
		          url:"/envia",
		          data:{
		          		"nombre":$nombre,
		                "_token":_token
		              },
		          type:"POST",
		          dataType:"text",
		          success:function(response){
		            	
		            
		          },  
		          error:function(){
		            alert("no");
		          }
		        });
		     });
		</script>
	</body>
</html>